
// * you can run them from command line (npm test-jest) 


// * Part 1 Creation of variables
// type script is strongly typed language
// it means that you have to declare type of variable
// for number you can use number keyword
// 
test('Declaration example ', () => {
   // let is one of the keywords in javascript to create variables
   // result is variable name
   // = is assignment operator
   // 2 is value of variable
   let result: number=2;
   // this is assertion we will talk about it later
   expect(result).toBe(2);
});

// * boolean  is simples type in typescript
// it can have only two values true or false
// test('Boolean variable example', () => {
//    let isTrue: boolean = true;
//    expect(isTrue).toBe(true);
// });

// * string is type for text values
// test('String variable example', () => {
//    let message: string = "Hello, World!";
//    expect(message).toBe("Hello, World!");
// });

// * You can also do math on strings and add numbers to strings
// * string is type for text values
// test('add 22 to end end beginning of string', () => {
//    let message: string = "Hello, World!";
//    let result: string = // you code
//    expect(result).toBe("22Hello, World!22");
// });

