
// * you can run them from command line (npm test-jest) 

// * Part 1 Creation of variables
test('You can create Variable using word let', () => {
    // let is one of the keywords in javascript to create variables
    // result is variable name
    // = is assignment operator
    // 2 is value of variable
    let result = 2;
    
    // this is assertion we will talk about it later
    expect(result).toBe(2);
});
// ! run the code (npm test-jest)

// ! uncomment next part and follow the instructions
// hint: you can uncomment whole block of text by selecting it and click  ⌘K then ⌘U
test('Create your own variable named myFirstVar with value 5', () => {
    let myFirstVar = 5;

    expect(myFirstVar).toBe(5);
});


// ! one more time
test('this time name it currentYear and set if with current Year', () => {
//todo check why it worked without let
    let currentYear = 2024;
    expect(currentYear).toBe(2024);
});

// * outside of let there is also var keyword
// * var is older version of let
// * it is not recommended to use var
// * but it is good to know that it exists 
// * and you can find it in older code
// * it has some specific use cases but they are out of scope this course
 test('example of var', () => {

    var result=2;
    expect(result).toBe(2);
 });


 // * Part 2 using variables

 test('you can change variable value after it is created ', () => {
    let result=2;
    result=3;
    result=4;
    expect(result).toBe(4);
 });

 test('you can assign one variable to other', () => {
    let firstVar=2;
    firstVar=3;
    let secondVar=firstVar;
  
    expect(secondVar).toBe(firstVar);
    expect(firstVar).toBe(3);
 });

 test('in case of primitive values those variables are leaving separately', () => {
    let firstVar=2;
    firstVar=3;
    let secondVar=firstVar;
    firstVar=6;
    // ! what is value of each variable here?
    expect(secondVar).toBe(insertYourAnswerHere);
    expect(firstVar).toBe(insertYourAnswerHere);
 });
 
 let firstVar=2;

 test('let example', () => {


    expect(firstVar).toBe(2);
 });

 test('let example 2', () => {

    expect(firstVar).toBe(3);
 });


//  test('you can also do math ', () => {
//     let firstVar=2+4;
//     let secondVar=firstVar+1;
//     let thirdVar=firstVar+secondVar;


//     expect(firstVar).toBe(6);
//     expect(secondVar).toBe(7);
//     expect(thirdVar).toBe(13);
//  });

// * you can use / * - + operators to do math
//  test('you can also do math ', () => {
//     let firstVar=2+4;
//     let secondVar=firstVar+1;
//     let thirdVar=firstVar+secondVar;


//     expect(firstVar).toBe(6);
//     expect(secondVar).toBe(7);
//     expect(thirdVar).toBe(13);
//  })

// ! finish the mathematical expression
// test('Something simple for start ', () => {
//     let firstVar=6
//     let secondVar=???;
//     let result=???;


//     expect(result).toBe(-2);
//  })

 // * as you can see we can use also negative values in variables

//  test('now little harder ', () => {
//     let firstVar=6
//     let secondVar=???;
//     let result=???;


//     expect(result).toBe(66);
//  })

 // if you want to do decimal you can use . (dot) to separate decimal part
//  test('now little harder ', () => {
//     let firstVar=11
//     let secondVar=5   


//     expect(result).toBe(2.2);
//  })

//  test('this require more then one operation ', () => {
//     let firstVar=66
//     let secondVar=5   


//     expect(result).toBe(18.2);
//  })

 // * there is also % operator
    // * it is called modulo
    // * it is returning reminder of division
    // test('try using modulo ', () => {
    //     let firstVar=5
    //     let secondVar=2   
    
    
    //     expect(result).toBe(1);
    //  })



    test('constant exapmple', () => {
        const firstVar=2;
        let secondVar=firstVar;
       secondVar=3
        expect(secondVar).not.toEqual(firstVar);
        expect(firstVar).toBe(2);
     });